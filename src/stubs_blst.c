#include "blst.h"
#define CAML_NAME_SPACE
#include "ocaml_integers.h"
#include <caml/alloc.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/memory.h>
#include <caml/mlvalues.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// From ocaml-ctypes:
// https://github.com/ocamllabs/ocaml-ctypes/blob/9048ac78b885cc3debeeb020c56ea91f459a4d33/src/ctypes/ctypes_primitives.h#L110
#if SIZE_MAX == UINT64_MAX
#define ctypes_size_t_val Uint64_val
#define ctypes_copy_size_t integers_copy_uint64
#else
#error "No suitable OCaml type available for representing size_t values"
#endif

#define Blst_fr_val(v) ((blst_fr *)Data_custom_val(v))

#define Fr_val_k(v, k) (Blst_fr_val(Field(v, k)))

static struct custom_operations blst_fr_ops = {"blst_fr",
                                               custom_finalize_default,
                                               custom_compare_default,
                                               custom_hash_default,
                                               custom_serialize_default,
                                               custom_deserialize_default,
                                               custom_compare_ext_default,
                                               custom_fixed_length_default};

bool blst_fr_from_lendian(blst_fr *x, byte b[32]) {
  blst_scalar *s = (blst_scalar *)calloc(1, sizeof(blst_scalar));
  blst_scalar_from_lendian(s, b);
  bool is_ok = blst_scalar_fr_check(s);
  if (is_ok) {
    blst_fr_from_scalar(x, s);
  }
  free(s);
  return (is_ok);
}

void blst_lendian_from_fr(byte b[32], blst_fr *x) {
  blst_scalar *s = (blst_scalar *)calloc(1, sizeof(blst_scalar));
  blst_scalar_from_fr(s, x);
  blst_lendian_from_scalar(b, s);
  free(s);
}

CAMLprim value caml_blst_fr_from_lendian_stubs(value x, value b) {
  CAMLparam2(x, b);
  blst_fr *x_c = Blst_fr_val(x);
  byte *b_c = Bytes_val(b);
  bool res = blst_fr_from_lendian(x_c, b_c);
  CAMLreturn(Val_bool(res));
}

CAMLprim value caml_blst_lendian_from_fr_stubs(value b, value x) {
  CAMLparam2(b, x);
  blst_fr *x_c = Blst_fr_val(x);
  byte *b_c = Bytes_val(b);
  blst_lendian_from_fr(b_c, x_c);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_allocate_t_stubs(value unit) {
  CAMLparam1(unit);
  CAMLlocal1(block);
  block = caml_alloc_custom(&blst_fr_ops, sizeof(blst_fr), 0, 1);
  blst_fr *c = Blst_fr_val(block);
  memset(c, 0, sizeof(blst_fr));
  CAMLreturn(block);
}

CAMLprim value caml_allocate_t_array_stubs(value t, value size) {
  CAMLparam2(t, size);
  CAMLlocal1(block);
  int size_c = Int_val(size);
  block = caml_alloc_custom(&blst_fr_ops, sizeof(blst_fr) * size_c, 0, 1);
  blst_fr *block_c = Blst_fr_val(block);
  for (int i = 0; i < size_c; i++) {
    memcpy(block_c + i, Blst_fr_val(Field(t, i)), sizeof(blst_fr));
  }
  CAMLreturn(block);
}

CAMLprim value caml_blst_fr_add_stubs(value ret, value p1, value p2) {
  CAMLparam3(ret, p1, p2);
  blst_fr_add(Blst_fr_val(ret), Blst_fr_val(p1), Blst_fr_val(p2));
  CAMLreturn(Val_unit);
}

CAMLprim value caml_set_t_stubs(value res, value b) {
  CAMLparam2(res, b);
  blst_fr_from_lendian(Blst_fr_val(res), Bytes_val(b));
  CAMLreturn(Val_unit);
}

CAMLprim value caml_sum_t_stubs(value res, value array, value size) {
  CAMLparam3(res, array, size);
  int size_c = Int_val(size);
  blst_fr *res_c = Blst_fr_val(res);
  for (int i = 0; i < size_c; i++) {
    blst_fr_add(res_c, res_c, Blst_fr_val(Field(array, i)));
  }
  CAMLreturn(Val_unit);
}

CAMLprim value caml_sum_t_partial_stubs(value res, value t, value start,
                                        value length) {
  CAMLparam3(t, start, length);
  int start_c = Int_val(start);
  int length_c = Int_val(length);
  blst_fr *res_c = Blst_fr_val(res);
  for (int i = 0; i < length_c; i++) {
    blst_fr_add(res_c, res_c, Blst_fr_val(Field(t, start_c + i)));
  }
  CAMLreturn(Val_unit);
}

CAMLprim value caml_sum_t_array_stubs(value res, value t, value length) {
  CAMLparam3(res, t, length);
  int length_c = Int_val(length);
  blst_fr *res_c = Blst_fr_val(res);
  blst_fr *t_c = Blst_fr_val(t);
  for (int i = 0; i < length_c; i++) {
    blst_fr_add(res_c, res_c, t_c + i);
  }
  CAMLreturn(Val_unit);
}

CAMLprim value caml_sum_t_array_partial_stubs(value res, value t, value start,
                                              value length) {
  CAMLparam4(res, t, start, length);
  int start_c = Int_val(start);
  int length_c = Int_val(length);
  blst_fr *res_c = Blst_fr_val(res);
  blst_fr *t_c = Blst_fr_val(t);
  for (int i = 0; i < length_c; i++) {
    blst_fr_add(res_c, res_c, t_c + start_c + i);
  }
  CAMLreturn(Val_unit);
}
