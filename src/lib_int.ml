(* harcoded, can be changed *)
let nb_chunks = 4

let nb_tasks = 4

let nb_domains = 2

module Stubs = struct
  (* It is a C int *)
  type t

  (* It is a contiguous C array of int *)
  type t_array

  external allocate_t : unit -> t = "caml_allocate_t_stubs"

  external set_t : t -> int -> unit = "caml_set_t_stubs"

  external to_t_array : t array -> int -> t_array
    = "caml_allocate_t_array_stubs"

  external sum : t array -> int -> int = "caml_sum_t_stubs"

  external sum_partial : t array -> int -> int -> int
    = "caml_sum_t_partial_stubs"

  external sum_t_array : t_array -> int -> int = "caml_sum_t_array_stubs"

  external sum_t_array_partial : t_array -> int -> int -> int
    = "caml_sum_t_array_partial_stubs"
end

let with_pool f =
  let name = "pool" in
  let pool =
    Domainslib.Task.setup_pool ~num_additional_domains:(nb_domains - 1) ~name ()
  in
  let res = f pool in
  Domainslib.Task.teardown_pool pool ;
  res

type t = Stubs.t

type t_array = Stubs.t_array * int

let allocate_t () = Stubs.allocate_t ()

let random_t () =
  let r = Random.int 2 in
  let v = allocate_t () in
  Stubs.set_t v r ;
  v

let allocate_t_array l =
  let length = Array.length l in
  (Stubs.to_t_array l length, length)

let t_array_length (_, n) = n

let sum l = Stubs.sum l (Array.length l)

let sum_chunks l =
  let size = Array.length l in
  assert (size >= nb_chunks) ;
  let chunk_size = size / nb_chunks in
  let rest = size mod nb_chunks in
  let rec aux i acc =
    if i = nb_chunks then
      if rest <> 0 then
        let start = i * chunk_size in
        let length = rest in
        let res = Stubs.sum_partial l start length in
        res :: acc
      else acc
    else
      let start = i * chunk_size in
      let length = chunk_size in
      let res = Stubs.sum_partial l start length in
      let acc = res :: acc in
      aux (i + 1) acc
  in
  List.fold_left ( + ) 0 (aux 0 [])

let sum_chunks_multicore l =
  let open Domainslib in
  with_pool (fun pool ->
      let size = Array.length l in
      let chunk_size = size / nb_tasks in
      let rest = size mod nb_tasks in
      assert (size >= chunk_size) ;
      let rec aux i_task acc =
        if i_task < 0 then acc
        else
          let start = i_task * chunk_size in
          let length =
            if i_task = nb_tasks - 1 then chunk_size + rest else chunk_size
          in
          let task =
            Task.async pool (fun _ -> Stubs.sum_partial l start length)
          in
          aux (i_task - 1) (task :: acc)
      in
      let ps = aux (nb_tasks - 1) [] in
      let l = List.map (Task.await pool) ps in
      List.fold_left ( + ) 0 l)

let sum_t_array (l, n) = Stubs.sum_t_array l n

let sum_t_array_chunks (l, size) =
  let chunk_size = size / nb_chunks in
  let rest = size mod nb_chunks in
  let rec aux i acc =
    if i = nb_chunks then
      if rest <> 0 then
        let start = i * chunk_size in
        let length = rest in
        let res = Stubs.sum_t_array_partial l start length in
        res :: acc
      else acc
    else
      let start = i * chunk_size in
      let length = chunk_size in
      let res = Stubs.sum_t_array_partial l start length in
      let acc = res :: acc in
      aux (i + 1) acc
  in
  List.fold_left ( + ) 0 (aux 0 [])

let sum_t_array_chunks_multicore (l, size) =
  let open Domainslib in
  let chunk_size = size / nb_tasks in
  let rest = size mod nb_tasks in
  let res =
    with_pool (fun pool ->
        let rec aux i_task acc =
          if i_task < 0 then acc
          else
            let start = i_task * chunk_size in
            let length =
              if i_task = nb_tasks - 1 then chunk_size + rest else chunk_size
            in
            let task =
              Task.async pool (fun _ ->
                  Stubs.sum_t_array_partial l start length)
            in
            aux (i_task - 1) (task :: acc)
        in
        let ps = aux (nb_tasks - 1) [] in
        let l = List.map (Task.await pool) ps in
        List.fold_left ( + ) 0 l)
  in
  res
