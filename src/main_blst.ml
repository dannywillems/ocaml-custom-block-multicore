let () = Random.self_init ()

let with_time f =
  let () = Gc.full_major () in
  let start_time = Sys.time () in
  let res = f () in
  let end_time = Sys.time () in
  ((end_time -. start_time) *. 1_000_000., res)

(* t array full sum *)
let () =
  let n = 1 lsl 24 in
  let array = Array.init n (fun _ -> Lib_blst.random_t ()) in
  let (time_res, res) = with_time (fun () -> Lib_blst.sum array) in
  let (time_res_partial, res_partial) =
    with_time (fun () -> Lib_blst.sum_chunks array)
  in
  let (time_res_partial_multicore, res_partial_multicore) =
    with_time (fun () -> Lib_blst.sum_chunks_multicore array)
  in
  print_endline "Caml array of Caml custom block of foo_t C values" ;
  Printf.printf
    "res = %s (time = %fus), res partial = %s (time = %fus), res partial \
     multicore = %s (time = %fus)\n"
    (Lib_blst.to_string res)
    time_res
    (Lib_blst.to_string res_partial)
    time_res_partial
    (Lib_blst.to_string res_partial_multicore)
    time_res_partial_multicore ;
  let t_array = Lib_blst.allocate_t_array array in
  let (time_res, res) = with_time (fun () -> Lib_blst.sum_t_array t_array) in
  let (time_res_partial, res_partial) =
    with_time (fun () -> Lib_blst.sum_t_array_chunks t_array)
  in
  let (time_res_partial_multicore, res_partial_multicore) =
    with_time (fun () -> Lib_blst.sum_t_array_chunks_multicore t_array)
  in
  print_endline "Caml custom block to a C array of foo_t values" ;
  Printf.printf
    "res = %s (time = %fus), res partial = %s (time = %fus), res partial \
     multicore = %s (time = %fus)\n"
    (Lib_blst.to_string res)
    time_res
    (Lib_blst.to_string res_partial)
    time_res_partial
    (Lib_blst.to_string res_partial_multicore)
    time_res_partial_multicore
