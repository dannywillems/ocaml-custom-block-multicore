(* harcoded, can be changed *)
let nb_chunks = 4

let nb_tasks = 4

let nb_domains = 2

let with_pool f =
  let pool =
    Domainslib.Task.setup_pool ~num_additional_domains:3 ~name:"pool" ()
  in
  let res = f pool in
  Domainslib.Task.teardown_pool pool ;
  res

module Stubs = struct
  (* It is a blst_fr value *)
  type t

  (* It is a contiguous C array of blst_fr values *)
  type t_array

  external to_string : Bytes.t -> t -> unit = "caml_blst_lendian_from_fr_stubs"

  external allocate_t : unit -> t = "caml_allocate_t_stubs"

  external add : t -> t -> t -> unit = "caml_blst_fr_add_stubs"

  external set_t : t -> Bytes.t -> unit = "caml_set_t_stubs"

  external to_t_array : t array -> int -> t_array
    = "caml_allocate_t_array_stubs"

  external sum : t -> t array -> int -> unit = "caml_sum_t_stubs"

  external sum_partial : t -> t array -> int -> int -> unit
    = "caml_sum_t_partial_stubs"

  external sum_t_array : t -> t_array -> int -> unit = "caml_sum_t_array_stubs"

  external sum_t_array_partial : t -> t_array -> int -> int -> unit
    = "caml_sum_t_array_partial_stubs"
end

type t = Stubs.t

type t_array = Stubs.t_array * int

let allocate_t () = Stubs.allocate_t ()

let to_string t =
  let b = Bytes.make 32 '\000' in
  Stubs.to_string b t ;
  Z.(to_string (of_bits (Bytes.to_string b)))

let random_t () =
  let r = Bytes.init 32 (fun _ -> char_of_int @@ Random.int 256) in
  let v = allocate_t () in
  Stubs.set_t v r ;
  v

let allocate_t_array l =
  let length = Array.length l in
  (Stubs.to_t_array l length, length)

let t_array_length (_, n) = n

let sum l =
  let res = Stubs.allocate_t () in
  Stubs.sum res l (Array.length l) ;
  res

let sum_chunks l =
  let size = Array.length l in
  assert (size >= nb_chunks) ;
  let chunk_size = size / nb_chunks in
  let rest = size mod nb_chunks in
  let rec aux i acc =
    let res = Stubs.allocate_t () in
    if i = nb_chunks then
      if rest <> 0 then (
        let start = i * chunk_size in
        let length = rest in
        Stubs.sum_partial res l start length ;
        res :: acc)
      else acc
    else
      let start = i * chunk_size in
      let length = chunk_size in
      Stubs.sum_partial res l start length ;
      let acc = res :: acc in
      aux (i + 1) acc
  in
  let l = aux 0 [] in
  List.fold_left
    (fun res a ->
      Stubs.add res res a ;
      res)
    (Stubs.allocate_t ())
    l

let sum_chunks_multicore l =
  let open Domainslib in
  with_pool (fun pool ->
      let size = Array.length l in
      let chunk_size = size / nb_domains in
      let rest = size mod nb_domains in
      assert (size >= chunk_size) ;
      let rec aux nb_task acc =
        if nb_task < 0 then acc
        else
          let start = nb_task * chunk_size in
          let length =
            if nb_task = nb_domains - 1 then chunk_size + rest else chunk_size
          in
          let task =
            Task.async pool (fun _ ->
                let res = Stubs.allocate_t () in
                Stubs.sum_partial res l start length ;
                res)
          in
          aux (nb_task - 1) (task :: acc)
      in
      let ps = aux (nb_tasks - 1) [] in
      let l = List.map (Task.await pool) ps in
      List.fold_left
        (fun res a ->
          Stubs.add res res a ;
          res)
        (Stubs.allocate_t ())
        l)

let sum_t_array (l, n) =
  let res = Stubs.allocate_t () in
  Stubs.sum_t_array res l n ;
  res

let sum_t_array_chunks (l, size) =
  let chunk_size = size / nb_chunks in
  let rest = size mod nb_chunks in
  let rec aux i acc =
    let res = Stubs.allocate_t () in
    if i = nb_chunks then
      if rest <> 0 then (
        let start = i * chunk_size in
        let length = rest in
        Stubs.sum_t_array_partial res l start length ;
        res :: acc)
      else acc
    else
      let start = i * chunk_size in
      let length = chunk_size in
      Stubs.sum_t_array_partial res l start length ;
      let acc = res :: acc in
      aux (i + 1) acc
  in
  let l = aux 0 [] in
  List.fold_left
    (fun res a ->
      Stubs.add res res a ;
      res)
    (Stubs.allocate_t ())
    l

let sum_t_array_chunks_multicore (l, size) =
  let open Domainslib in
  with_pool (fun pool ->
      let chunk_size = size / nb_domains in
      let rest = size mod nb_domains in
      let rec aux i_task acc =
        if i_task < 0 then acc
        else
          let start = i_task * chunk_size in
          let length =
            if i_task = nb_tasks - 1 then chunk_size + rest else chunk_size
          in
          let res = Stubs.allocate_t () in
          let task =
            Task.async pool (fun _ ->
                Stubs.sum_t_array_partial res l start length ;
                res)
          in
          aux (i_task - 1) (task :: acc)
      in
      let ps = aux (nb_tasks - 1) [] in
      let l = List.map (Task.await pool) ps in
      List.fold_left
        (fun res a ->
          Stubs.add res res a ;
          res)
        (Stubs.allocate_t ())
        l)
