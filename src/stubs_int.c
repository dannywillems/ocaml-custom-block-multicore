#define CAML_NAME_SPACE
#include <caml/alloc.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/memory.h>
#include <caml/mlvalues.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE_FOO 80

typedef struct {
  int i[SIZE_FOO];
} foo_t;

#define Foo_val(v) ((foo_t *)Data_custom_val(v))

#define Foo_array_val(v) ((foo_t *)Data_custom_val(v))

#define Foo_val_k(v, k) (Foo_val(Field(v, k)))

static struct custom_operations foo_ops = {"t",
                                           custom_finalize_default,
                                           custom_compare_default,
                                           custom_hash_default,
                                           custom_serialize_default,
                                           custom_deserialize_default,
                                           custom_compare_ext_default,
                                           custom_fixed_length_default};

static struct custom_operations foo_array_ops = {"t_array",
                                                 custom_finalize_default,
                                                 custom_compare_default,
                                                 custom_hash_default,
                                                 custom_serialize_default,
                                                 custom_deserialize_default,
                                                 custom_compare_ext_default,
                                                 custom_fixed_length_default};

CAMLprim value caml_allocate_t_stubs(value unit) {
  CAMLparam1(unit);
  CAMLlocal1(block);
  block = caml_alloc_custom(&foo_ops, sizeof(foo_t), 0, 1);
  memset(Foo_val(block), 0, sizeof(foo_t));
  CAMLreturn(block);
}

CAMLprim value caml_set_t_stubs(value t, value v) {
  CAMLparam2(t, v);
  int v_c = Int_val(v);
  foo_t *t_c = Foo_val(t);
  for (int i = 0; i < SIZE_FOO; i++) {
    t_c->i[i] = v_c;
  }
  CAMLreturn(Val_unit);
}

CAMLprim value caml_allocate_t_array_stubs(value t, value size) {
  CAMLparam2(t, size);
  CAMLlocal1(block);
  int size_c = Int_val(size);
  block = caml_alloc_custom(&foo_array_ops, sizeof(foo_t) * size_c, 0, 1);
  foo_t *block_c = Foo_array_val(block);
  for (int i = 0; i < size_c; i++) {
    foo_t *tmp = Foo_val_k(t, i);
    memcpy(block_c + i, tmp, sizeof(foo_t));
  }
  CAMLreturn(block);
}

CAMLprim value caml_sum_t_stubs(value t, value size) {
  CAMLparam2(t, size);
  int size_c = Int_val(size);
  int res = 0;
  for (int i = 0; i < size_c; i++) {
    for (int j = 0; j < SIZE_FOO; j++) {
      foo_t *tmp = Foo_val_k(t, i);
      res += tmp->i[j];
    }
  }
  CAMLreturn(Val_int(res));
}

CAMLprim value caml_sum_t_partial_stubs(value t, value start, value length) {
  CAMLparam3(t, start, length);
  int start_c = Int_val(start);
  int length_c = Int_val(length);
  int res = 0;
  for (int i = 0; i < length_c; i++) {
    for (int j = 0; j < SIZE_FOO; j++) {
      foo_t *tmp = Foo_val_k(t, i + start_c);
      res += tmp->i[j];
    }
  }
  CAMLreturn(Val_int(res));
}

CAMLprim value caml_sum_t_array_stubs(value t, value length) {
  CAMLparam2(t, length);
  int length_c = Int_val(length);
  int res = 0;
  foo_t *t_c = Foo_array_val(t);
  for (int i = 0; i < length_c; i++) {
    for (int j = 0; j < SIZE_FOO; j++) {
      foo_t tmp = t_c[i];
      res += tmp.i[j];
    }
  }
  CAMLreturn(Val_int(res));
}

CAMLprim value caml_sum_t_array_partial_stubs(value t, value start,
                                              value length) {
  CAMLparam3(t, start, length);
  int start_c = Int_val(start);
  int length_c = Int_val(length);
  int res = 0;
  foo_t *t_c = Foo_array_val(t);
  for (int i = 0; i < length_c; i++) {
    for (int j = 0; j < SIZE_FOO; j++) {
      foo_t tmp = t_c[i + start_c];
      res += tmp.i[j];
    }
  }
  CAMLreturn(Val_int(res));
}
