type t

type t_array

val allocate_t : unit -> t

val random_t : unit -> t

val allocate_t_array : t array -> t_array

val t_array_length : t_array -> int

val sum : t array -> t

val sum_chunks : t array -> t

val sum_chunks_multicore : t array -> t

val sum_t_array : t_array -> t

val sum_t_array_chunks : t_array -> t

val sum_t_array_chunks_multicore : t_array -> t

val to_string : t -> string
