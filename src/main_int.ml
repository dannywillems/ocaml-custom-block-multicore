let () = Random.self_init ()

let n = 1 lsl 22

let array = Array.init n (fun _ -> Lib_int.random_t ())

let t_array = Lib_int.allocate_t_array array

module StringMap = Map.Make (String)

let results : (int * float) StringMap.t = StringMap.empty

let with_time f =
  (* let () = Gc.full_major () in *)
  let start_time = Sys.time () in
  let res = f () in
  let end_time = Sys.time () in
  (res, (end_time -. start_time) *. 1_000_000.)

let () =
  let f () = Lib_int.sum array in
  let results = StringMap.add "Single core, Caml array" (with_time f) results in
  let f () = Lib_int.sum_chunks array in
  let results =
    StringMap.add "Single core, Caml array chunks" (with_time f) results
  in
  let f () = Lib_int.sum_chunks_multicore array in
  let results = StringMap.add "Multi core, Caml array" (with_time f) results in
  let f () = Lib_int.sum_t_array t_array in
  let results = StringMap.add "Single core, C array" (with_time f) results in
  let f () = Lib_int.sum_t_array_chunks t_array in
  let results =
    StringMap.add "Single core, C array chunks" (with_time f) results
  in
  let f () = Lib_int.sum_t_array_chunks_multicore t_array in
  let results = StringMap.add "Multi core, C array" (with_time f) results in
  (* Getting the results and comparing *)
  let values = List.map fst (List.map snd (StringMap.bindings results)) in
  assert (List.length values >= 1) ;
  StringMap.iter (fun desc (x, _) -> Printf.printf "%s: %d\n" desc x) results ;
  let exp_res = List.hd values in
  assert (List.for_all (fun x -> exp_res = x) values) ;
  StringMap.iter
    (fun desc (_res, time) -> Printf.printf "%s: %fms\n" desc time)
    results ;
  ()
