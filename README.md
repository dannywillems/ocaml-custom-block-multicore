# OCaml custom blocks multicore


```
opam switch create ./ 4.12.0+domains --repositories=multicore=git+https://github.com/ocaml-multicore/multicore-opam.git,default
opam install merlin domainslib ocamlformat
dune exec ./src/main.exe
```

